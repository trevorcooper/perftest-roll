PKGROOT         = /opt/ior
NAME		= ior
VERSION		= 2.10.3
RELEASE		= 0
TARBALL_POSTFIX	= tgz
RPM.EXTRAS	= "AutoReq: no"
COPYRIGHT	= Copyright (c) 2003, The Regents of the University of California.
